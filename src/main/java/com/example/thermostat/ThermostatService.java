package com.example.thermostat;

import com.example.thermostat.model.Thermostat;
import com.example.thermostat.model.ThermostatMode;
import com.example.thermostat.representation.request.ThermostatRequest;
import com.example.thermostat.representation.response.ThermostatMinimalResponse;
import com.example.thermostat.representation.response.ThermostatResponse;
import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityNotFoundException;
import org.h2.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ThermostatService {

  private final ThermostatRepository repository;

  @Autowired
  public ThermostatService(ThermostatRepository repository) {

    this.repository = repository;
  }

  public ThermostatMinimalResponse createThermostat(ThermostatRequest request) {
    return transformToThermostatMinimalResponse(save(transformToEntity(request)));
  }

  public ThermostatMinimalResponse updateThermostat(long id, ThermostatRequest request) {
    Thermostat updatedThermostat = findById(id);
    updatedThermostat.setManufacturer(request.getManufacturer());
    updatedThermostat.setSetPoint(request.getSetPoint());
    updatedThermostat.setThermostatMode(request.getThermostatMode());
    return transformToThermostatMinimalResponse(save(updatedThermostat));
  }

  public void deleteThermostatById(long id) {
    Thermostat thermostat = findById(id);
    repository.delete(thermostat);
  }

  public ThermostatResponse findThermostatById(long id) {

    return transformToThermostatResponse(findById(id));
  }

  public List<ThermostatResponse> getThermostatList(String thermostatMode) {
    List<Thermostat> thermostatList = (StringUtils.isNullOrEmpty(thermostatMode)) ? repository.findAll() :
        repository.findAllByThermostatMode(ThermostatMode.findByStateMode(thermostatMode));
    return thermostatList.stream()
        .map(this::transformToThermostatResponse)
        .collect(Collectors.toList());
  }

  private Thermostat transformToEntity(ThermostatRequest request) {
    return Thermostat.builder()
        .manufacturer(request.getManufacturer())
        .setPoint(request.getSetPoint())
        .thermostatMode(request.getThermostatMode())
        .build();
  }

  private ThermostatMinimalResponse transformToThermostatMinimalResponse(Thermostat thermostat) {
    return ThermostatMinimalResponse.builder()
        .id(thermostat.getId())
        .build();
  }

  private ThermostatResponse transformToThermostatResponse(Thermostat thermostat) {
    return ThermostatResponse.builder()
        .id(thermostat.getId())
        .manufacturer(thermostat.getManufacturer())
        .setPoint(thermostat.getSetPoint())
        .thermostatMode(thermostat.getThermostatMode())
        .build();
  }

  private Thermostat save(Thermostat thermostat) {

    return repository.save(thermostat);
  }

  private Thermostat findById(long id) {
    return repository.findById(id).orElseThrow(() ->
        new EntityNotFoundException(MessageFormat.format("Thermostat with id: {0}", id)));
  }
}
