package com.example.thermostat.representation.response;

import com.example.thermostat.model.ThermostatMode;
import com.example.thermostat.representation.response.ThermostatMinimalResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ThermostatResponse extends ThermostatMinimalResponse {

  @JsonProperty("manufacturer")
  private String manufacturer;
  @JsonProperty("setPoint")
  private Float setPoint;
  @JsonProperty("thermostatMode")
  private ThermostatMode thermostatMode;
}
