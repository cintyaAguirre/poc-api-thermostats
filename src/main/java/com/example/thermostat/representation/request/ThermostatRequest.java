package com.example.thermostat.representation.request;

import com.example.thermostat.model.ThermostatMode;
import com.fasterxml.jackson.annotation.JsonProperty;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.springframework.validation.annotation.Validated;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class ThermostatRequest {

  @JsonProperty("manufacturer")
  @NotBlank(message = "manufacturer is mandatory")
  private String manufacturer;

  @JsonProperty("setPoint")
  @NotNull(message = "setPoint is mandatory")
  private Float setPoint;

  @JsonProperty("thermostatMode")
  @NotNull(message = "thermostatMode is mandatory")
  private ThermostatMode thermostatMode;
}
