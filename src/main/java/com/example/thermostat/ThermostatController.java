package com.example.thermostat;

import com.example.thermostat.representation.request.ThermostatRequest;
import com.example.thermostat.representation.response.ThermostatMinimalResponse;
import com.example.thermostat.representation.response.ThermostatResponse;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ThermostatController implements ThermostatApi {

  private static final String DEFAULT_HOME_RESPONSE_MESSAGE = "Spring is here!";
  private final ThermostatService service;

  @Autowired
  public ThermostatController(ThermostatService service) {
    this.service = service;
  }

  @Override
  public String home() {

    return DEFAULT_HOME_RESPONSE_MESSAGE;
  }

  @Override
  public ThermostatMinimalResponse createThermostat(@Valid ThermostatRequest request) {

    return service.createThermostat(request);
  }

  @Override
  public ThermostatResponse getThermostatById(long id) {

    return service.findThermostatById(id);
  }

  @Override
  public List<ThermostatResponse> getThermostatList(String thermostatMode) {
    return service.getThermostatList(thermostatMode);
  }

  @Override
  public ThermostatMinimalResponse updateThermostatById(long id, @Valid ThermostatRequest request) {

    return service.updateThermostat(id, request);
  }

  @Override
  public void deleteThermostatById(long id) {
    service.deleteThermostatById(id);
  }
}
