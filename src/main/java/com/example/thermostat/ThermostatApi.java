package com.example.thermostat;

import com.example.thermostat.representation.request.ThermostatRequest;
import com.example.thermostat.representation.response.ThermostatMinimalResponse;
import com.example.thermostat.representation.response.ThermostatResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Api(tags = "Thermostats Information")
public interface ThermostatApi {

  @ApiOperation(value = "GET root info", nickname = "getRootInfo")
  @GetMapping(value = "/")
  @ResponseStatus(HttpStatus.OK)
  String home();

  @ApiOperation(value = "Register Thermostat Data.", nickname = "createThermostat")
  @ApiResponses(value = {
      @ApiResponse(code = 201, message = "Register Thermostat OK", response = ThermostatMinimalResponse.class),
      @ApiResponse(code = 400, message = "Bad request. Check if the request meets the API requirements."),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 404, message = "The specified resource was not found"),
      @ApiResponse(code = 500, message = "Unexpected error")})
  @PostMapping(value = "/thermostats",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.CREATED)
  ThermostatMinimalResponse createThermostat(@Valid @RequestBody ThermostatRequest request);

  @ApiOperation(value = "Edit Thermostat Data by Id.", nickname = "updateThermostatById")
  @ApiResponses(value = {
      @ApiResponse(code = 204, message = "Edit Thermostat Data by Id OK"),
      @ApiResponse(code = 400, message = "Bad request. Check if the request meets the API requirements."),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 404, message = "The specified resource was not found"),
      @ApiResponse(code = 500, message = "Unexpected error")})
  @PutMapping(value = "/thermostats/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  ThermostatMinimalResponse updateThermostatById(@PathVariable long id, @Valid @RequestBody ThermostatRequest request);

  @ApiOperation(value = "Delete Thermostat Data by Id.", nickname = "deleteThermostatById")
  @ApiResponses(value = {
      @ApiResponse(code = 204, message = "Delete Thermostat Data by Id OK"),
      @ApiResponse(code = 400, message = "Bad request. Check if the request meets the API requirements."),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 404, message = "The specified resource was not found"),
      @ApiResponse(code = 500, message = "Unexpected error")})
  @DeleteMapping(value = "/thermostats/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  void deleteThermostatById(@PathVariable long id);

  @ApiOperation(value = "Get Thermostat Data by Id.", nickname = "getThermostatById")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Get Thermostat Data by Id OK", response = ThermostatResponse.class),
      @ApiResponse(code = 400, message = "Bad request. Check if the request meets the API requirements."),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 404, message = "The specified resource was not found"),
      @ApiResponse(code = 500, message = "Unexpected error")})
  @GetMapping(value = "/thermostats/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  ThermostatResponse getThermostatById(@PathVariable long id);

  @ApiOperation(value = "Get All Thermostat Data.", nickname = "getThermostatList")
  @ApiResponses(value = {
      @ApiResponse(code = 200, message = "Get Thermostat Data List OK", response = ThermostatResponse.class, responseContainer = "list"),
      @ApiResponse(code = 400, message = "Bad request. Check if the request meets the API requirements."),
      @ApiResponse(code = 401, message = "Unauthorized"),
      @ApiResponse(code = 404, message = "The specified resource was not found"),
      @ApiResponse(code = 500, message = "Unexpected error")})
  @GetMapping(value = "/thermostats", produces = {MediaType.APPLICATION_JSON_VALUE})
  @ResponseStatus(HttpStatus.OK)
  List<ThermostatResponse> getThermostatList(
      @ApiParam(value = "Thermostat mode search parameter") @Valid @RequestParam(required = false, defaultValue = "") String thermostatMode);
}
