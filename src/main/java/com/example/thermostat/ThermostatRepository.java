package com.example.thermostat;

import com.example.thermostat.model.Thermostat;
import com.example.thermostat.model.ThermostatMode;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ThermostatRepository extends JpaRepository<Thermostat, Long> {

  List<Thermostat> findAllByThermostatMode(ThermostatMode thermostatMode);
}
