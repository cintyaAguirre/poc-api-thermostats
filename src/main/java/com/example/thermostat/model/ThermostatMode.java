package com.example.thermostat.model;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;

@Getter
public enum ThermostatMode {
  HEATING("heating"),
  COOLING("cooling"),
  OFF("off");

  private static final Map<String, ThermostatMode> BY_STATE_NAME = new HashMap<>();

  static {
    for (ThermostatMode e : values()) {
      BY_STATE_NAME.put(e.stateMode, e);
    }
  }

  private final String stateMode;

  ThermostatMode(String stateMode) {

    this.stateMode = stateMode;
  }

  public static ThermostatMode findByStateMode(String stateMode) {

    return BY_STATE_NAME.get(stateMode.toLowerCase());
  }
}
