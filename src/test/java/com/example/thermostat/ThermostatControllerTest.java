package com.example.thermostat;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.thermostat.model.Thermostat;
import com.example.thermostat.util.DataGeneratorUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.skyscreamer.jsonassert.Customization;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.skyscreamer.jsonassert.comparator.CustomComparator;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@Import(ThermostatTestContextConfiguration.class)
@RunWith(SpringRunner.class)
public class ThermostatControllerTest {

  private static final String CONTEXT_PATH = "/thermostats";
  private static final String THERMOSTAT_ID_CONTEXT_PATH = "/thermostats/1";
  private MockMvc mockMvc;
  private ObjectMapper objectMapper;

  @Mock
  private ThermostatRepository repository;

  @InjectMocks
  private ThermostatService service;

  @Before
  public void setup() {
    objectMapper = new ObjectMapper();
    this.mockMvc = MockMvcBuilders.standaloneSetup(new ThermostatController(service)).build();
    when(repository.save(any(Thermostat.class))).then(returnsFirstArg());
    when(repository.findById(any(Long.class))).thenReturn(Optional.of(DataGeneratorUtil.generateValidThermostat()));
  }

  @Test
  public void whenRightPathResourceInCreateThermostat_ThenShouldReturn201() throws Exception {
    mockMvc.perform(post(CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(DataGeneratorUtil.generateValidThermostatRequest())))
        .andExpect(status().isCreated());
  }

  @Test
  public void whenValidRequestAndRightPathResourceInCreateThermostat_ThenShouldReturnValidResponse() throws Exception {
    MockHttpServletResponse response = mockMvc.perform(post(CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(DataGeneratorUtil.generateValidThermostatRequest())))
        .andReturn().getResponse();

    String expectedJson = objectMapper.writeValueAsString(DataGeneratorUtil.generateValidThermostatMinimalResponse());
    String actualJson = response.getContentAsString();
    JSONAssert.assertEquals(expectedJson, actualJson,
        new CustomComparator(
            JSONCompareMode.STRICT,
            new Customization("id", (o1, o2) -> true)));
  }

  @Test
  public void whenRightPathResourceInGetThermostatById_ThenShouldReturn200() throws Exception {
    mockMvc.perform(get(THERMOSTAT_ID_CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void whenRightPathResourceInGetThermostatById_ThenShouldReturnValidResponse() throws Exception {
    MockHttpServletResponse response = mockMvc.perform(get(THERMOSTAT_ID_CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON))
        .andReturn().getResponse();

    String expectedJson = objectMapper.writeValueAsString(DataGeneratorUtil.generateValidThermostatResponse());
    String actualJson = response.getContentAsString();
    JSONAssert.assertEquals(expectedJson, actualJson,
        new CustomComparator(
            JSONCompareMode.STRICT,
            new Customization("id", (o1, o2) -> true)));
  }

  @Test
  public void whenValidRequestAndRightPathResourceInUpdateThermostat_ThenShouldReturn200() throws Exception {
    mockMvc.perform(put(THERMOSTAT_ID_CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(DataGeneratorUtil.generateValidThermostatRequest())))
        .andExpect(status().isOk());
  }

  @Test
  public void whenRightPathResourceInDeleteThermostat_ThenShouldReturn204() throws Exception {
    mockMvc.perform(delete(THERMOSTAT_ID_CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNoContent());
  }

  @Test
  public void whenRightPathResourceInGetThermostatList_ThenShouldReturn200() throws Exception {
    mockMvc.perform(get(CONTEXT_PATH)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
}