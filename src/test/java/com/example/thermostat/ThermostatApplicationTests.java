package com.example.thermostat;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@Import(ThermostatTestContextConfiguration.class)
@RunWith(SpringRunner.class)
public class ThermostatApplicationTests {

  private MockMvc mockMvc;

  @Mock
  private ThermostatRepository repository;

  @InjectMocks
  private ThermostatService service;

  @Before
  public void setup() {

    this.mockMvc = MockMvcBuilders.standaloneSetup(new ThermostatController(service)).build();
  }

  @Test
  public void homeResponse() throws Exception {
    mockMvc.perform(get("/"))
        .andExpect(status().isOk());
  }
}
