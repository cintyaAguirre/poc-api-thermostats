package com.example.thermostat;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
@ExtendWith(MockitoExtension.class)
public class ThermostatTestContextConfiguration {

  @Mock
  private ThermostatRepository repository;

  @Bean
  public ThermostatService thermostatService() {

    return new ThermostatService(repository);
  }
}
