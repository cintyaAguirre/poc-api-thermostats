package com.example.thermostat.util;

import com.example.thermostat.model.Thermostat;
import com.example.thermostat.model.ThermostatMode;
import com.example.thermostat.representation.request.ThermostatRequest;
import com.example.thermostat.representation.response.ThermostatMinimalResponse;
import com.example.thermostat.representation.response.ThermostatResponse;

public final class DataGeneratorUtil {

  private DataGeneratorUtil() {

  }

  public static ThermostatRequest generateValidThermostatRequest() {
    return ThermostatRequest.builder()
        .manufacturer("manufacturer 1")
        .setPoint(20.5F)
        .thermostatMode(ThermostatMode.HEATING)
        .build();
  }

  public static Thermostat generateValidThermostat() {
    return Thermostat.builder()
        .id(1L)
        .manufacturer("manufacturer 1")
        .setPoint(20.5F)
        .thermostatMode(ThermostatMode.HEATING)
        .build();
  }

  public static ThermostatMinimalResponse generateValidThermostatMinimalResponse() {
    return ThermostatMinimalResponse.builder()
        .id(1L)
        .build();
  }

  public static ThermostatResponse generateValidThermostatResponse() {
    return ThermostatResponse.builder()
        .id(1L)
        .manufacturer("manufacturer 1")
        .setPoint(20.5F)
        .thermostatMode(ThermostatMode.HEATING)
        .build();
  }
}
